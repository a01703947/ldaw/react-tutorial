import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

var Square = function(props){
  return (
    <button className="square" onClick={props.onClick}>
      {props.value === 'X' && (<span className='x'>X</span>)}
      {props.value === 'O' && (<span className='o'>O</span>)}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return <Square
      value={this.props.squares[i]}
      onClick={() => this.props.onClick(i)}
      />;
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null)
      }],
      stepNumber: 0,
      xIsNext: true,
    }
  }

  jumpTo(step){
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    });
  }

  handleClick(i){
    const history = this.state.history.slice(
      0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if(this.calculateWinner() || squares[i]) return;
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }

  calculateWinner(){
    const history = this.state.history;
    let squares = history[this.state.stepNumber].squares;
    const LINES = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];

    for(let i = 0; i < LINES.length; i++){
      const [A, B, C] = LINES[i];
      if(
        squares[A] &&
        squares[A] === squares[B] &&
        squares[B] === squares[C]
      ){
        return squares[A];
      }
    }
      return null;
  }

  showHistory(){
    let historyMenu = document.getElementsByClassName('game-history')[0];
    let historyToggleButton = document.getElementsByClassName(
      'game-history-toggle'
    )[0];
    historyToggleButton.classList.toggle("active");
    historyMenu.classList.toggle("active");
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];

    const moves = history.map((step, move) => {
      const desc = move ? `Go to move #${move}` : "Go to game start";
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    return (
      <div className="game">
        <div className="game-header">
          <div className="game-status"> 
            { !this.calculateWinner() && ('Now playing:')}
            { this.calculateWinner() && ('Winner:')}
            &nbsp;
            { this.state.xIsNext 
              && !this.calculateWinner()
              && (<span className="x">X</span>) }
            { !this.state.xIsNext 
              && !this.calculateWinner()
              && (<span className="o">O</span>) }
            { this.calculateWinner() === 'X'
              && (<span className="x">X</span>) }
            { this.calculateWinner() === 'O'
              && (<span className="o">O</span>) }
            </div>
          <div className="game-history-toggle"
            onClick={() => this.showHistory()}></div>
          <div className="game-history">
            <ol>{moves}</ol>
          </div>
        </div>
        <div className="game-title">
          <h1>Tic-Tac-Toe</h1>
        </div>
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

